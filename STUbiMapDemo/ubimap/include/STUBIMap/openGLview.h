//
//  openGLview.h
//  OpenGL-ES
//
//  Created by 刘涛 on 15/11/12.
//  Copyright © 2015年 ShiTu. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, RenderMode) {
    RenderModeTexture = 0,
    RenderModeVectogram,
};

@interface openGLview : UIView

@property(strong,nonatomic)UIImage *imageMap;

@property(strong,nonatomic)NSString *filePath;//map文件二级路径~~/name.type 读取文件

//相应数据读取
- (instancetype)initWithFrame:(CGRect)frame WithFileStoreId:(NSInteger)storeId AndRenderMode:(RenderMode)mode;
//传坐标
- (void)movePoint:(CGPoint)point;
//传角度
- (void)moveAngle:(float)angle;
//换楼层
- (void)changeCurrentBuildingWithFloorIndex:(NSInteger)floor andMap:(UIImage *)map;
//跟随模式
- (void)followMode;
//停止OpenGL
- (void)stopDraw;

@end
