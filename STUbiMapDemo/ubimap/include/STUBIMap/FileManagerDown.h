//
//  FileManagerDown.h
//  OpenGL-ES
//
//  Created by 刘涛 on 16/1/27.
//  Copyright © 2016年 ShiTu. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol fileDownDelegate <NSObject>

@required

- (void)ResourceFileDownloadStatus;

- (void)mapFileDownloadStatus:(NSInteger)storeid;

@end

typedef void (^statusBlock)(void);

@interface FileManagerDown : NSObject

@property(weak,nonatomic)id<fileDownDelegate>fileDelegate;

@property(weak,nonatomic)statusBlock status;

/**
 *  清楚缓存文件
 */
+ (void)removeMapAndZipfile;

///**
// *  检查资源文件
// */
//- (void)checkTheResourceFile;
///**
// *  提取map文件，有则读取，无则下载
// *
// *  @param storeId 商厦id
// */
//- (void)loadingMapZipWithStoreID:(NSInteger)storeId;
///**
// *  检查资源文件版本号
// *
// *  @param ver 版本号
// *
// *  @return YES则需要更新，NO则无需更新
// */
//- (BOOL)checkResourceVersionNumber:(NSString *)ver;
///**
// *  检查地图文件版本号
// *
// *  @param idx storeid
// *
// *  @param ver 版本号
// *
// *  @return YES则需要更新，NO则无需更新
// */
//- (BOOL)checkMapVersionNumber:(NSInteger)idx AndVer:(NSString *)ver;

/**
 *  检查资源文件版本号
 *
 *  @param ver 版本号
 */
- (void)checkResourceVersion:(NSString *)ver andStatus:(statusBlock)complete;
/**
 *  检查地图文件版本号
 *
 *  @param idx storeid
 *  @param ver 版本号
 */
- (void)checkMapVersion:(NSInteger)idx AndVer:(NSString *)ver andStatus:(statusBlock)complete;

@end
