//
//  ViewController.m
//  STUbiMapDemo
//
//  Created by 刘涛 on 16/1/22.
//  Copyright © 2016年 ShiTu. All rights reserved.
//

#import "ViewController.h"
#import "judgeNetwork.h"
#import "GLViewController.h"
#import "SetViewController.h"
#import "FileManagerDown.h"
#import "ListTableViewCell.h"

@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    GLViewController *gl;
}
@property(strong,nonatomic)UITableView *table;

@property(strong,nonatomic)NSArray *data;

@property(strong,nonatomic)NSDictionary *dic;

@property(strong,nonatomic)FileManagerDown *fd;

@property(strong,nonatomic)UIView *waitBack;

@property(strong,nonatomic)UIImageView *waitImage;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self layoutView];
    [self initData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (![[NSUserDefaults standardUserDefaults] valueForKey:@"mapAzip"]) {
        [self showWait];
        [_fd checkResourceVersion:[_dic valueForKey:@"res"] andStatus:^{
            [self endWait];
        }];
    }
}

#pragma -mark tableview delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _data.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = @"cell";
    ListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[ListTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    cell.titleImage.image = [UIImage imageNamed:@"building"];
    cell.nameLabel.text = [[_data objectAtIndex:indexPath.row] valueForKey:@"name"];
    cell.addressLabel.text = [[_data objectAtIndex:indexPath.row] valueForKey:@"address"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    gl = [[GLViewController alloc] init];
    gl.storeId = [[[_data objectAtIndex:indexPath.row] valueForKey:@"storeid"] integerValue];
    gl.titleStr = [[_data objectAtIndex:indexPath.row] valueForKey:@"name"];
    //检查map版本号
    if ([judgeNetwork judgeNetworkState]) {
        [self showWait];
        [_fd checkMapVersion:gl.storeId AndVer:[_data objectAtIndex:indexPath.row] andStatus:^{
            [self endWait];
            [self.navigationController pushViewController:gl animated:YES];
        }];
    }else{
        NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:@"/Map/"];
        NSString *idx = [[NSString stringWithFormat:@"%ld",gl.storeId] stringByAppendingString:@".idx"];
        NSString *ubi = [[NSString stringWithFormat:@"%ld",gl.storeId] stringByAppendingString:@".ubi"];
        NSFileManager *fm = [NSFileManager defaultManager];
        if ([fm fileExistsAtPath:[path stringByAppendingString:idx]]&&[fm fileExistsAtPath:[path stringByAppendingString:ubi]]) {
            [self.navigationController pushViewController:gl animated:YES];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90.f;
}
#pragma -mark response events
- (void)jumpAbout{
    SetViewController *set = [[SetViewController alloc] init];
    [self.navigationController pushViewController:set animated:YES];
}
#pragma -mark layoutView and data
- (void)layoutView{
    self.navigationController.navigationBarHidden = YES;
    UIView *viewbar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    viewbar.backgroundColor = [UIColor colorWithRed:0 green:186.0/255.0 blue:240.0/255.0 alpha:1.f];
    [self.view addSubview:viewbar];
    
    _table = [[UITableView alloc] initWithFrame:CGRectMake(0, 64.f, self.view.frame.size.width, self.view.frame.size.height-64.f) style:UITableViewStylePlain];
    _table.delegate = self;
    _table.dataSource = self;
    _table.separatorStyle = NO;
    [self.view addSubview:_table];
    _data = [NSArray array];
    
    UIButton *right = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-45.f, 20.f, 44.f, 44.f)];
    [right setImage:[UIImage imageNamed:@"personal_setting_icon"] forState:UIControlStateNormal];
    [right addTarget:self action:@selector(jumpAbout) forControlEvents:UIControlEventTouchUpInside];
    [viewbar addSubview:right];
    
    
    UIImage *imagetit = [UIImage imageNamed:@"title"];
    float W = CGImageGetWidth(imagetit.CGImage)/2.0;
    float H = CGImageGetHeight(imagetit.CGImage)/2.0;
    UIImageView *imageTitle = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, W, H)];
    imageTitle.center = CGPointMake(self.view.frame.size.width/2.0,44.f);
    imageTitle.image = imagetit;
    [self.view addSubview:imageTitle];
}

- (void)initData{
    _fd = [[FileManagerDown alloc] init];

    if ([judgeNetwork judgeNetworkState]) {
        NSURL *url = [NSURL URLWithString:@"http://www.ubirouting.com/ubimaps/ubimap.json"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
            _dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            _data = [_dic valueForKey:@"maps"];
            NSLog(@"%@",_data);
            [NSKeyedArchiver archiveRootObject:_dic toFile:[self getDocumentPath]];
            [_table reloadData];
        }];
    }else{
        //读取本地
        _dic = [NSKeyedUnarchiver unarchiveObjectWithFile:[self getDocumentPath]];
        _data = [_dic valueForKey:@"maps"];
    }
    
    //检查res版本号
    if ([judgeNetwork judgeNetworkState]) {
        [self showWait];
        [_fd checkResourceVersion:[_dic valueForKey:@"res"] andStatus:^{
            [self endWait];
        }];
    }
}
- (NSString *)getDocumentPath{
    return [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:@"/data.json"];
}
- (void)showWait{
    [self.view addSubview:self.waitBack];
    [self waitAnimition];
}
- (void)endWait{
    [_waitImage.layer removeAllAnimations];
    [self.waitBack removeFromSuperview];
}
- (void)waitAnimition{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 ];
    rotationAnimation.duration = 1.f;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 65535;
    [_waitImage.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}
- (UIView *)waitBack{
    if (!_waitBack) {
        _waitBack = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _waitBack.backgroundColor = [UIColor blackColor];
        _waitBack.alpha = 0.4f;
        UIImage *image = [UIImage imageNamed:@"login_circle"];
        _waitImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 105, 105)];
        _waitImage.image = image;
        _waitImage.center = _waitBack.center;
        [_waitBack addSubview:_waitImage];
        UILabel *waitLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, _waitImage.frame.origin.y+110.f, self.view.frame.size.width, 30)];
        waitLabel.textAlignment = NSTextAlignmentCenter;
        waitLabel.text = @"加载中...";
        waitLabel.textColor = [UIColor whiteColor];
        [_waitBack addSubview:waitLabel];
    }
    return _waitBack;
}

@end
