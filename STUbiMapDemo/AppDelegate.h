//
//  AppDelegate.h
//  STUbiMapDemo
//
//  Created by 刘涛 on 16/1/22.
//  Copyright © 2016年 ShiTu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

