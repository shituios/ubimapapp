//
//  main.m
//  STUbiMapDemo
//
//  Created by 刘涛 on 16/1/22.
//  Copyright © 2016年 ShiTu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
