//
//  judgeNetwork.m
//  nevxinwen
//
//  Created by liutao on 15/6/11.
//  Copyright (c) 2015年 LML. All rights reserved.
//

#import "judgeNetwork.h"


@implementation judgeNetwork
//检测网络是否连接
+(BOOL)judgeNetworkState{
    if ([self connectedToNetwork]) {
        return YES;
    }else{
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"异常提示" message:@"网络连接状态异常" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        alert.alpha=0.3;
        [alert show];
        return NO;
    }
}

+(BOOL) connectedToNetwork
{
    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    if (!didRetrieveFlags)
    {
        printf("Error. Could not recover network reachability flags\n");
        return NO;
    }
    BOOL isReachable = ((flags & kSCNetworkFlagsReachable) != 0);
    BOOL needsConnection = ((flags & kSCNetworkFlagsConnectionRequired) != 0);
    return (isReachable && !needsConnection) ? YES : NO;
}

@end
