//
//  judgeNetwork.h
//  nevxinwen
//
//  Created by liutao on 15/6/11.
//  Copyright (c) 2015年 LML. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonHMAC.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <netdb.h>
#import <arpa/inet.h>

@interface judgeNetwork : NSObject

+(BOOL)judgeNetworkState;

@end
