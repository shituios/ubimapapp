//
//  ListTableViewCell.m
//  STUbiMapDemo
//
//  Created by 刘涛 on 16/2/19.
//  Copyright © 2016年 ShiTu. All rights reserved.
//

#import "ListTableViewCell.h"
#define HEIGHT 90.f

@implementation ListTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self  = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _titleImage = [[UIImageView alloc] initWithFrame:CGRectMake(22, 15, 60, 60)];
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(100.f, 25.f, self.frame.size.width-122, 18.f)];
        _nameLabel.textColor = [UIColor colorWithRed:34.f/255.f green:34.f/255.f blue:34.f/255.f alpha:1.f];
        [_nameLabel setFont:[UIFont systemFontOfSize:18.f]];

        _addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(_nameLabel.frame.origin.x, _nameLabel.frame.origin.y+_nameLabel.frame.size.height+12.f, _nameLabel.frame.size.width, 13.f)];
        _addressLabel.textColor = [UIColor colorWithRed:102.f/255.f green:102.f/255.f blue:102.f/255.f alpha:1.f];
        [_addressLabel setFont:[UIFont systemFontOfSize:13.f]];
        [self.contentView addSubview:_titleImage];
        [self.contentView addSubview:_nameLabel];
        [self.contentView addSubview:_addressLabel];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(_titleImage.frame.origin.x, 89.f, [UIScreen mainScreen].bounds.size.width-44.f, 1.f)];
        line.backgroundColor = [UIColor colorWithRed:230.f/255.f green:230.f/255.f blue:230.f/255.f alpha:1.f];
        [self.contentView addSubview:line];
        
        UIImageView *access = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pocket_arrow_icon"]];
        access.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-56.f, 35.f, 20.f, 20.f);
        [self.contentView addSubview:access];
    }
    return self;
}

@end
