//
//  GLViewController.m
//  STUbiMapDemo
//
//  Created by 刘涛 on 16/1/28.
//  Copyright © 2016年 ShiTu. All rights reserved.
//

#import "GLViewController.h"
#import "openGLview.h"

@interface GLViewController ()

@property(strong,nonatomic)openGLview *openGL;


@end

@implementation GLViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    UIView *viewbar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    viewbar.backgroundColor = [UIColor colorWithRed:0 green:186.0/255.0 blue:240.0/255.0 alpha:1.f];
    [self.view addSubview:viewbar];
    
    UILabel *titleLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 44)];
    titleLab.text = _titleStr;
    titleLab.textColor = [UIColor whiteColor];
    titleLab.textAlignment = NSTextAlignmentCenter;
    titleLab.backgroundColor = [UIColor clearColor];
    [self.view addSubview:titleLab];
    
    UIButton *left = [[UIButton alloc] initWithFrame:CGRectMake(0, 20.f, 40.f, 40.f)];
    [left setImage:[UIImage imageNamed:@"buildinginfo_back_icon"] forState:UIControlStateNormal];
    [left addTarget:self action:@selector(jumpBack) forControlEvents:UIControlEventTouchUpInside];
    [viewbar addSubview:left];
    
    _openGL = [[openGLview alloc] initWithFrame:CGRectMake(0, 64.f, self.view.frame.size.width, self.view.frame.size.height-64.f) WithFileStoreId:_storeId AndRenderMode:RenderModeVectogram];
    [self.view addSubview:_openGL];

}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)jumpBack{
    [_openGL stopDraw];
    [self.navigationController popViewControllerAnimated:YES];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
