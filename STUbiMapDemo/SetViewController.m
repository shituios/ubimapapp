//
//  SetViewController.m
//  STUbiMapDemo
//
//  Created by 刘涛 on 16/1/29.
//  Copyright © 2016年 ShiTu. All rights reserved.
//

#import "SetViewController.h"
#import "AboutViewController.h"
#import "FileManagerDown.h"

@interface SetViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>

@property(strong,nonatomic)UITableView *table;

@property(strong,nonatomic)NSArray *dataAry;

@end

@implementation SetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIView *viewbar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    viewbar.backgroundColor = [UIColor colorWithRed:0 green:186.0/255.0 blue:240.0/255.0 alpha:1.f];
    [self.view addSubview:viewbar];
    UILabel *titleLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 44)];
    titleLab.text = @"设置";
    titleLab.textColor = [UIColor whiteColor];
    titleLab.textAlignment = NSTextAlignmentCenter;
    [viewbar addSubview:titleLab];
    
    UIButton *left = [[UIButton alloc] initWithFrame:CGRectMake(0, 20.f, 40.f, 40.f)];
    [left setImage:[UIImage imageNamed:@"buildinginfo_back_icon"] forState:UIControlStateNormal];
    [left addTarget:self action:@selector(jumpBack) forControlEvents:UIControlEventTouchUpInside];
    [viewbar addSubview:left];
    
    _table = [[UITableView alloc] initWithFrame:CGRectMake(0, 64.f, self.view.frame.size.width, self.view.frame.size.height-64.f) style:UITableViewStyleGrouped];
    _table.delegate = self;
    _table.dataSource = self;
    [self.view addSubview:_table];
    
    _dataAry = [NSArray arrayWithObjects:@"关于我们",@"清理缓存",nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _dataAry.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *indentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifier];
    }
    cell.textLabel.text = [_dataAry objectAtIndex:indexPath.section];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section==0) {
        AboutViewController *about = [[AboutViewController alloc] init];
        [self.navigationController pushViewController:about animated:YES];
    }else if (indexPath.section==1){
        UIAlertView *aler = [[UIAlertView alloc] initWithTitle:@"提示" message:@"清除后，地图文件将重新加载..." delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [aler show];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [FileManagerDown removeMapAndZipfile];
    }
}

- (void)jumpBack{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
