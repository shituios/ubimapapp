//
//  AboutViewController.m
//  STUbiMapDemo
//
//  Created by 刘涛 on 16/1/28.
//  Copyright © 2016年 ShiTu. All rights reserved.
//

#import "AboutViewController.h"

static NSString *contect = @"该套矢量图库由北京识途科技有限公司开发制作。\n\n识途UbiMap是一套矢量地图整体解决方案，提供Android及iOS SDK。同时，具备高效的矢量地图制作手段，丰富的素材库，灵活的配色和主题定制方案，致力于为用户提供矢量地图的绘制和管理服务。\n\n矢量地图特性：具备旋转、缩放、路径规划、楼层切换、弹窗等功能，是实现室内定位、室内搜索功能的前提条件。\n\n矢量地图种类：商场、停车场、机场候机楼、火车站候车室、会展中心、办公楼、景区、新建楼盘等。";
@interface AboutViewController ()
{
    UIWebView * phoneCallWebView;
    float fontsysteml;
}
@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([UIScreen mainScreen].bounds.size.height>735.f) {
        fontsysteml = 18.f;
    }else if ([UIScreen mainScreen].bounds.size.height>666.f){
        fontsysteml = 16.f;
    }else{
        fontsysteml = 14.f;
    }
    self.view.backgroundColor = [UIColor whiteColor];
    UIView *viewbar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    viewbar.backgroundColor = [UIColor colorWithRed:0 green:186.0/255.0 blue:240.0/255.0 alpha:1.f];
    [self.view addSubview:viewbar];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 44)];
    title.backgroundColor = [UIColor clearColor];
    title.textAlignment = NSTextAlignmentCenter;
    title.text = @"关于识途UbiMap";
    title.textColor = [UIColor whiteColor];
    [viewbar addSubview:title];
    
    UIButton *left = [[UIButton alloc] initWithFrame:CGRectMake(0, 20.f, 40.f, 40.f)];
    [left setImage:[UIImage imageNamed:@"buildinginfo_back_icon"] forState:UIControlStateNormal];
    [left addTarget:self action:@selector(jumpBack) forControlEvents:UIControlEventTouchUpInside];
    [viewbar addSubview:left];

    UITextView *text = [[UITextView alloc] initWithFrame:CGRectMake(3.f, 64, self.view.frame.size.width-6.f, self.view.frame.size.height/2.0)];
    text.editable = NO;
    text.text = contect;
    text.font = [UIFont systemFontOfSize:fontsysteml];
    [self.view addSubview:text];
    
    [self layoutContectView];

}
- (void)jumpBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)CallPhone{
    NSString *phoneNum = @"18611341854";// 电话号码
    NSURL *phoneURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneNum]];
    if (!phoneCallWebView ){
        phoneCallWebView = [[UIWebView alloc]initWithFrame:CGRectZero];// 这个webView只是一个后台的容易 不需要add到页面上来 效果跟方法二一样 但是这个方法是合法的
    }
    [phoneCallWebView loadRequest:[NSURLRequest requestWithURL:phoneURL]];
}

- (void)openURL{
    
    NSURL *url = [NSURL URLWithString:@"http://www.ubirouting.com"];
    [[UIApplication sharedApplication] openURL:url];
    
}
- (void)layoutContectView{
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(3.f, self.view.frame.size.height*2.0/3.0, self.view.frame.size.width-6.f, self.view.frame.size.height/2.0-64.f)];
    backView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:backView];
    
    float hei = backView.frame.size.height/6.f;
    float dexhei = 0;//backView.frame.size.height/30.f;
    UILabel *titLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, backView.frame.size.width, backView.frame.size.height/6.f)];
    titLabel.text = @"欲了解详情，您可通过如下方式联系我们：";
    [titLabel setFont:[UIFont systemFontOfSize:fontsysteml]];
    [backView addSubview:titLabel];
    
    UILabel *qqLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, hei+dexhei, backView.frame.size.width, hei)];
    qqLabel.text = @"官方QQ: 3314478190";
    [qqLabel setFont:[UIFont systemFontOfSize:fontsysteml]];
    [backView addSubview:qqLabel];
    
    UILabel *emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, hei*2+dexhei*2, backView.frame.size.width, hei)];
    emailLabel.text = @"邮件地址：contact@ubirouting.com";
    [emailLabel setFont:[UIFont systemFontOfSize:fontsysteml]];
    [backView addSubview:emailLabel];
    
    UIButton *telbtn = [[UIButton alloc] initWithFrame:CGRectMake(0, (hei+dexhei)*3.f, backView.frame.size.width, hei)];
    NSMutableAttributedString *teltitle = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"联系电话：18611341854（李先生)"]];
    NSRange contentRange = {5,11};
    [teltitle addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:contentRange];
    [teltitle addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0 green:186.0/255.0 blue:240.0/255.0 alpha:1.f] range:contentRange];
    [telbtn setAttributedTitle:teltitle forState:UIControlStateNormal];
    [telbtn.titleLabel setFont:[UIFont systemFontOfSize:fontsysteml]];
    [telbtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [telbtn addTarget:self action:@selector(CallPhone) forControlEvents:UIControlEventTouchUpInside];
    [backView addSubview:telbtn];
    
    
    UIButton *shituUrl = [[UIButton alloc] initWithFrame:CGRectMake(0, (hei+dexhei)*4.f, backView.frame.size.width, hei)];
    NSMutableAttributedString *urltitle = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"网址：www.ubirouting.com"]];
    NSRange urlRange = {3,18};
    [urltitle addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:urlRange];
    [urltitle addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0 green:186.0/255.0 blue:240.0/255.0 alpha:1.f] range:urlRange];
    [shituUrl setAttributedTitle:urltitle forState:UIControlStateNormal];
    [shituUrl.titleLabel setFont:[UIFont systemFontOfSize:fontsysteml]];
    [shituUrl setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [shituUrl addTarget:self action:@selector(openURL) forControlEvents:UIControlEventTouchUpInside];
    [backView addSubview:shituUrl];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
