//
//  ListTableViewCell.h
//  STUbiMapDemo
//
//  Created by 刘涛 on 16/2/19.
//  Copyright © 2016年 ShiTu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListTableViewCell : UITableViewCell

@property(strong,nonatomic)UIImageView *titleImage;

@property(strong,nonatomic)UILabel *nameLabel;

@property(strong,nonatomic)UILabel *addressLabel;

@end
